'''
Template Component main class.

'''

import logging
import os
import sys
from pathlib import Path
import requests
import base64
import pandas as pd
import json
import time
import datetime
import dateparser

from kbc.env_handler import KBCEnvHandler

# configuration variables
KEY_OUTPUT_BUCKET = 'outputBucket'
KEY_CLIENT_ID = 'client_id'
KEY_CLIENT_SECRET = '#client_secret'
KEY_ENDPOINT = 'endpoint'

# #### Keep for debug
KEY_DEBUG = 'debug'

# list of mandatory parameters => if some is missing, component will fail with readable message on initialization.
MANDATORY_PARS = [
    KEY_OUTPUT_BUCKET,
    KEY_CLIENT_ID,
    KEY_CLIENT_SECRET,
    KEY_ENDPOINT
]
MANDATORY_IMAGE_PARS = []

OUTPUT_PATH = '../data/out/tables/'

APP_VERSION = '0.0.6'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        # for easier local project setup
        default_data_dir = Path(__file__).resolve().parent.parent.joinpath('data').as_posix() \
            if not os.environ.get('KBC_DATADIR') else None

        KBCEnvHandler.__init__(self, MANDATORY_PARS, log_level=logging.DEBUG if debug else logging.INFO,
                               data_path=default_data_dir)
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True
        if debug:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            # validation of mandatory parameters. Produces ValueError
            self.validate_config(MANDATORY_PARS)
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params

        # Authentication
        client_id = params.get(KEY_CLIENT_ID)
        client_secret = params.get(KEY_CLIENT_SECRET)
        self.token = self.authenticate(client_id, client_secret)
        self.outputBucket = params.get(KEY_OUTPUT_BUCKET) if params.get(
            KEY_OUTPUT_BUCKET) else 'genesys'

        # Endpoints
        endpoint = params.get(KEY_ENDPOINT)
        logging.info(f'Parsing {endpoint}')

        # Query
        aggregate_endpoint = True if 'aggregate' in endpoint else False
        all_query = self._construct_query(
            interval=params.get("interval"),
            groupby=params.get("group_by"),
            metric=params.get("metric"),
            filter_in=params.get("filter"),
            aggregate_endpoint=aggregate_endpoint,
            granularity=params.get("granularity"))
        logging.info(f'Input Query: {all_query}')

        # Caching output tables columns orders
        self.columns = []

        for query in all_query:
            # all_users:
            if endpoint == "all_users":
                self.fetch_all_users()
                break

            # routing_queues
            elif endpoint == "routing_queues":
                self.fetch_routing_queues()
                break

            # user_status_details_query
            elif endpoint == "user_status_detail_query":
                all_users = self.fetch_all_users(aggregation=True)
                self.fetch_user_status_detail_query(query, all_users=all_users)

            # user_status_aggregate_query
            elif endpoint == "user_status_aggregate_query":
                self.fetch_user_status_aggregate_query(query)

            # conversation_detail_query
            elif endpoint == "conversation_detail_query":
                self.fetch_conversation_detail_query(query)

            # conversation_aggregate_query
            elif endpoint == "conversation_aggregate_query":
                self.fetch_conversation_aggregate_query(query)

            # user_detail_job
            elif endpoint == "user_detail_job":
                self.fetch_job(endpoint, query)

            elif endpoint == "conversation_detail_job":
                self.fetch_job(endpoint, query)

    def _construct_query(self, interval, granularity,
                         groupby=None, metric=None, filter_in=None, aggregate_endpoint=False):

        all_queries = []
        query = {}

        # Aggregate endpoints
        if aggregate_endpoint:
            '''
            if not groupby or not metric:
                raise Exception(
                    "Aggregate parameters are missing: groupBy, metric, filter")
            '''
            if groupby:
                query["groupBy"] = groupby
            if metric:
                query["metrics"] = metric
            if filter_in:
                query["filter"] = filter_in[0]
            query["granularity"] = granularity

        # Interval
        if interval["start_date"] == '' or interval["end_date"] == '':
            start_date = dateparser.parse("yesterday")
            end_date = dateparser.parse("today")
        else:
            start_date = dateparser.parse(interval["start_date"])
            end_date = dateparser.parse(interval["end_date"])

            if start_date > end_date:
                raise Exception("Invalid Interval")

        all_intervals = self._break_interval(
            start_date=start_date, end_date=end_date, granularity=granularity)

        for i in all_intervals:
            tmp = query.copy()
            tmp["interval"] = i
            all_queries.append(tmp)

        return all_queries

    def _break_interval(self, start_date, end_date, granularity):

        limit = 1000
        param = {
            "PT30M": 48,
            "PT1H": 24,
            "P1D": 1
        }

        all_intervals = []
        interval_val = param[granularity]
        interval_loop = True
        request_day_limit = int(limit/param[granularity])

        while interval_loop:

            days_diff = (end_date-start_date).days
            total_request = days_diff * interval_val

            if total_request > limit:
                tmp_start_date = start_date.strftime('%Y-%m-%d')
                tmp_end_date = (
                    start_date + datetime.timedelta(days=request_day_limit)).strftime('%Y-%m-%d')
                all_intervals.append(
                    f"{tmp_start_date}T06:00:00.000Z/{tmp_end_date}T06:00:00.000Z")

            else:
                tmp_start_date = start_date.strftime('%Y-%m-%d')
                tmp_end_date = end_date.strftime('%Y-%m-%d')
                all_intervals.append(
                    f"{tmp_start_date}T06:00:00.000Z/{tmp_end_date}T06:00:00.000Z")
                interval_loop = False

            # resetting looping values
            start_date = start_date + \
                datetime.timedelta(days=request_day_limit)

        return all_intervals

    def authenticate(self, client_id, client_secret):

        authorization = base64.b64encode(
            bytes(client_id + ":" + client_secret, "ISO-8859-1")).decode("ascii")
        authorization_headers = {
            "Authorization": f"Basic {authorization}",
            "Content-Type": "application/x-www-form-urlencoded"
        }
        authorization_body = {
            "grant_type": "client_credentials"
        }

        # Fetch token
        auth = requests.post("https://login.mypurecloud.com/oauth/token",
                             data=authorization_body, headers=authorization_headers)

        # Fail to connect
        if auth.status_code != 200:
            raise Exception('Failed to fetch access token.')

        return auth.json()["access_token"]

    def fetch_endpoint(self, endpoint, query):

        request_url = f"https://api.mypurecloud.com/api/v2/analytics/{endpoint}"
        request_header = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {self.token}",
            "Accept": "application/json"
        }
        request_body = query

        response = requests.post(request_url, data=json.dumps(
            request_body), headers=request_header)

        if response.status_code not in [200, 202]:
            raise Exception(f'Request Failed: {response.json()}')

        return response.json()

    def output_table(self, data, tablename, primary_key):

        # Output
        output_df = pd.DataFrame(data)

        # Incremental Output
        if os.path.exists(f"{OUTPUT_PATH}{tablename}.csv"):
            output_df.to_csv(f"{OUTPUT_PATH}{tablename}.csv",
                             index=False, header=False, mode='a', columns=self.columns)

        else:
            # Caching output columns orders
            self.columns = list(output_df.columns)

            logging.info(f'Outputting {tablename}')
            output_df.to_csv(f"{OUTPUT_PATH}{tablename}.csv",
                             index=False, columns=self.columns)

            # Manifest
            manifest = {
                "primary_key": primary_key,
                "incremental": True,
                "destination": f'in.c-{self.outputBucket}.{tablename}'
            }
            with open(f"{OUTPUT_PATH}{tablename}.csv.manifest", "w") as f:
                json.dump(manifest, f)

    def parse_user_data(self, data):
        # Output Tables and Primary Keys
        output_table = []
        primary_key = ["userId", "organizationPresenceId", "startTime"]

        # No data
        if "userDetails" not in data:
            logging.info("No data found.")
            return

        # Parsing
        for user in data["userDetails"]:

            if "primaryPresence" in user:

                for presence in user["primaryPresence"]:
                    data = {
                        "userId": user["userId"],
                        "organizationPresenceId": presence["organizationPresenceId"],
                        "routingStatus": "",
                        "startTime": presence.get("startTime"),
                        "endTime": presence.get("endTime"),
                        "systemPresence": presence.get("systemPresence"),
                    }

                    output_table.append(data)

            if "routingStatus" in user:

                for routing in user["routingStatus"]:
                    data = {
                        "userId": user["userId"],
                        "organizationPresenceId": "",
                        "routingStatus": routing.get("routingStatus"),
                        "startTime": routing.get("startTime"),
                        "endTime": routing.get("endTime"),
                        "systemPresence": ""
                    }

                    output_table.append(data)

        self.output_table(
            output_table, tablename="user_status_detail", primary_key=primary_key)

    def parse_conversation_data(self, data):
        # Output Tables
        conversation_table = []
        participant_table = []
        session_table = []
        session_segment_table = []
        session_metric_table = []

        # Primary Keys
        conversation_primary_key = ["conversationId"]
        participant_primary_key = ["conversationId", "participantId"]
        session_primary_key = ["conversationId", "participantId", "sessionId"]
        session_segment_primary_key = [
            "conversationId", "participantId", "sessionId", "segmentType"]
        session_metric_primary_key = [
            "conversationId", "participantId", "sessionId", "metric"]

        # No data
        if "conversations" not in data:
            logging.info("No data found.")
            sys.exit(0)

        for convo in data["conversations"]:

            conversation_data = {
                "conversationId": convo.get("conversationId"),
                "conversationStart": convo.get("conversationStart"),
                "conversationEnd": convo.get("conversationEnd"),
                "mediaStatsMinConversationMos": convo.get("mediaStatsMinConversationMos"),
                "originatingDirection": convo.get("originatingDirection"),
                "mediaStatsMinConversationRFactor": convo.get("mediaStatsMinConversationRFactor"),
                "divisionIds": f"{convo.get('divisionIds')}"
            }

            conversation_table.append(conversation_data)

            for participant in convo["participants"]:

                participant_data = {
                    "conversationId": convo.get("conversationId"),
                    "participantId": participant.get("participantId"),
                    "participantName": participant.get("participantName"),
                    "userId": participant.get("userId"),
                    "purpose": participant.get("purpose")
                }

                participant_table.append(participant_data)

                for session in participant["sessions"]:

                    session_data = {
                        "conversationId": convo.get("conversationId"),
                        "participantId": participant.get("participantId"),
                        "sessionId": session.get("sessionId"),
                        "mediaType": session.get("mediaType"),
                        "ani": session.get("ani"),
                        "direction": session.get("direction"),
                        "dnis": session.get("dnis"),
                        "sessionDnis": session.get("sessionDnis"),
                        "edgeId": session.get("edgeId"),
                        "remote": session.get("remote"),
                        "remoteNameDisplayable": session.get("remoteNameDisplayable"),
                        "protocolCallId": session.get("protocolCallId"),
                        "provider": session.get("protocolCallId")
                    }

                    session_table.append(session_data)

                    # Segments
                    if "segments" in session:
                        for segment in session["segments"]:
                            segment_data = {
                                "conversationId": convo.get("conversationId"),
                                "participantId": participant.get("participantId"),
                                "sessionId": session.get("sessionId"),
                                "segmentStart": segment.get("segmentStart"),
                                "segmentEnd": segment.get("segmentEnd"),
                                "disconnectType": segment.get("disconnectType"),
                                "segmentType": segment.get("segmentType"),
                                "conference": segment.get("conference")
                            }

                            session_segment_table.append(segment_data)

                    # Metrics
                    if "metrics" in session:
                        for metric in session["metrics"]:
                            metric_data = {
                                "conversationId": convo.get("conversationId"),
                                "participantId": participant.get("participantId"),
                                "sessionId": session.get("sessionId"),
                                "metric": metric.get("name"),
                                "value": metric.get("value"),
                                "emitData": metric.get("emitData")
                            }

                            session_metric_table.append(metric_data)

        self.output_table(conversation_table, tablename="conversation_detail",
                          primary_key=conversation_primary_key)
        self.output_table(participant_table, tablename="conversation_participant",
                          primary_key=participant_primary_key)
        self.output_table(
            session_table, tablename="conversation_session", primary_key=session_primary_key)
        self.output_table(session_segment_table, tablename="conversation_session_segment",
                          primary_key=session_segment_primary_key)
        self.output_table(session_metric_table, tablename="conversation_session_metric",
                          primary_key=session_metric_primary_key)

    def fetch_all_users(self, aggregation=False):

        endpoint = "https://api.mypurecloud.com/api/v2/users"
        header = {
            "Authorization": f"Bearer {self.token}"
        }

        response = requests.get(endpoint, headers=header)

        # Output tables
        user_table = []
        user_primary_contact_table = []
        user_address_table = []

        # Priamry Keys
        user_primary_key = ["id"]
        user_primary_contact_primary_key = ["user_id", "mediaType", "type"]
        user_address_primary_key = ["user_id", "mediaType", "type"]

        # No data
        if "entities" not in response.json():
            logging.info("No data found.")
            sys.exit(0)

        # Parsing
        for user in response.json()["entities"]:
            user_data = {
                "id": user["id"],
                "name": user.get("name"),
                "division_id": user["division"].get("id"),
                "division_name": user["division"].get("name"),
                "chat_jabberId": user["chat"].get("jabberiId"),
                "email": user["email"],
                "state": user["state"],
                "username": user["username"],
                "version": user["version"],
                "acdAutoAnswer": user["acdAutoAnswer"],
                "seflUri": user["selfUri"]
            }

            user_table.append(user_data)

            for address in user["addresses"]:
                address_data = {
                    "user_id": user["id"],
                    "type": address["type"],
                    "mediaType": address["mediaType"],
                    "display": address.get("display"),
                    "extension": address.get("extension"),
                    "countryCode": address.get("countryCode"),
                    "address": address.get("address")
                }

                user_address_table.append(address_data)

            for contact in user["primaryContactInfo"]:
                contact_data = {
                    "user_id": user["id"],
                    "type": contact["type"],
                    "mediaType": contact["mediaType"],
                    "address": contact.get("address"),
                    "display": contact.get("display")
                }

                user_primary_contact_table.append(contact_data)

        if not aggregation:
            self.output_table(user_table, tablename="user",
                              primary_key=user_primary_key)
            self.output_table(user_primary_contact_table,
                              tablename="user_primary_contact",
                              primary_key=user_primary_contact_primary_key)
            self.output_table(user_address_table, tablename="user_address",
                              primary_key=user_address_primary_key)
        else:
            return user_table

    def fetch_routing_queues(self):

        endpoint = "https://api.mypurecloud.com/api/v2/routing/queues"

        header = {
            "Authorization": f"Bearer {self.token}"
        }

        page_num = 1
        pagination_loop = True

        # Output Tables
        routing_queue_table = []
        routing_queue_mediaSettings = []

        routing_queue_primary_key = ["id"]
        routing_queue_mediaSettings_primary_key = ["entity_id", "media"]

        while pagination_loop:
            logging.info(f"Fetching Page [{page_num}]")

            params = {
                "pageNumber": page_num
            }

            response = requests.get(endpoint, headers=header, params=params)

            # Error response
            if response.status_code != 200:
                logging.error(f"Request Error: {response.text}")
                sys.exit(1)

            # Pagination Loop Validation
            if "pageCount" not in response.json() \
                    or response.json()["pageCount"] == page_num:
                pagination_loop = False

            # Parsing
            for entry in response.json()["entities"]:
                data = {
                    "id": entry["id"],
                    "name": entry["name"],
                    "division_id": entry["division"]["id"],
                    "division_name": entry["division"]["name"],
                    "division_selfUri": entry["division"]["selfUri"],
                    "dateModified": entry.get("dateModified"),
                    "modifiedBy": entry.get("modifiedBy"),
                    "memberCount": entry["memberCount"],
                    "acwSettings_wrapupPrompt": entry["acwSettings"]["wrapupPrompt"],
                    "skillEvaluationMethod": entry["skillEvaluationMethod"],
                    "autoAnswerOnly": entry["autoAnswerOnly"],
                    "defaultScripts": f'{entry["defaultScripts"]}',
                    "selfUri": entry["selfUri"]
                }

                # Media Settings
                media_data = []
                for media in entry["mediaSettings"]:

                    tmp_data = {
                        "entity_id": entry["id"],
                        "media": media,
                        "alertingTimeoutSeconds": entry["mediaSettings"][media]["alertingTimeoutSeconds"],
                        "serviceLevel_percentage": entry["mediaSettings"][media]["serviceLevel"]["percentage"],
                        "serviceLevel_durationMs": entry["mediaSettings"][media]["serviceLevel"]["durationMs"]
                    }

                    media_data.append(tmp_data)

                routing_queue_table.append(data)
                routing_queue_mediaSettings += media_data

            # Loop
            page_num += 1

        # Output
        self.output_table(routing_queue_table, tablename="routing_queue",
                          primary_key=routing_queue_primary_key)
        self.output_table(routing_queue_mediaSettings, tablename="routing_queue_mediaSettings",
                          primary_key=routing_queue_mediaSettings_primary_key)

    def fetch_user_status_detail_query(self, query, all_users):

        endpoint = "users/details/query"

        # Looping through all the users available
        for i in all_users:

            logging.info(f"Processing user [{i['id']}]")

            # Request Body
            userFilters = [
                {
                    "type": "or",
                    "predicates": [
                        {
                            "dimension": "userId",
                            "value": i["id"]
                        }
                    ]
                }
            ]
            query["userFilters"] = userFilters

            # API Request
            response = self.fetch_endpoint(endpoint, query)

            # Data parser
            self.parse_user_data(response)

    def fetch_user_status_aggregate_query(self, query):

        endpoint = "users/aggregates/query"

        response = self.fetch_endpoint(endpoint, query)

        output_table = []
        primary_key = query["groupBy"] + ["interval", "metric", "qualifier"]

        # Special system mapping returned from API
        # to map the names of the qualifiers
        if "systemToOrganizationMappings" in response:
            systemToOrganizationMappings = response["systemToOrganizationMappings"]
        else:
            systemToOrganizationMappings = {}

        # No data
        if "results" not in response:
            logging.info("No data found.")
            sys.exit(0)

        # Parsing
        for row in response["results"]:
            group = row["group"]

            for interval in row["data"]:

                for metric in interval["metrics"]:
                    tmp_data = {
                        "interval": interval["interval"],
                        "metric": metric["metric"],
                        "qualifier": metric["qualifier"],
                        "sum": metric["stats"]["sum"]
                    }

                    # Mapping Qualifiers
                    for i in systemToOrganizationMappings:
                        if tmp_data["qualifier"] in systemToOrganizationMappings[i]:
                            tmp_data["qualifier"] = i

                    # Concat with group values
                    data = {**group, **tmp_data}

                    output_table.append(data)

        self.output_table(
            output_table, tablename="user_status_aggregate", primary_key=primary_key)

    def fetch_conversation_detail_query(self, query):

        endpoint = "conversations/details/query"

        response = self.fetch_endpoint(endpoint, query)

        self.parse_conversation_data(response)

    def fetch_conversation_aggregate_query(self, query):

        endpoint = "conversations/aggregates/query"

        response = self.fetch_endpoint(endpoint, query)

        output_table = []
        primary_key = query["groupBy"] + ["interval", "metric"]

        # No data
        if "results" not in response:
            logging.info("No data found.")
            sys.exit(0)

        # Parsing
        for row in response["results"]:
            group = {}

            for i in row["group"]:
                group[i] = row["group"][i]

            for interval in row["data"]:

                for metric in interval["metrics"]:
                    tmp_data = {
                        "interval": interval["interval"],
                        "metric": metric["metric"],
                        "sum": metric["stats"].get("sum"),
                        "max": metric["stats"].get("max"),
                        "count": metric["stats"].get("count")
                    }

                    # Concat with group values
                    data = {**group, **tmp_data}

                    output_table.append(data)

        self.output_table(
            output_table, tablename="conversation_aggregate", primary_key=primary_key)

    def fetch_job(self, endpoint, query):

        if endpoint == "user_detail_job":
            endpoint_url = "users/details/jobs"
        elif endpoint == "conversation_detail_job":
            endpoint_url = "conversations/details/jobs"

        # getting job ID
        response = self.fetch_endpoint(endpoint_url, query)
        jobId = response["jobId"]
        logging.info(f"JobId: [{jobId}]")

        job_url = f"https://api.mypurecloud.com/api/v2/analytics/{endpoint_url}/{jobId}"
        header = {
            "Authorization": f"Bearer {self.token}"
        }

        while True:
            job_response = requests.get(job_url, headers=header)

            if job_response.json()["state"] in ["QUEUED", "PENDING"]:
                logging.info(
                    f"Job request state: {job_response.json()['state']}")
                time.sleep(5)

            elif job_response.json()["state"] in ["FAILED", "CANCELLED", "EXPIRED"]:
                logging.error("Job request failed.")
                sys.exit(1)

            elif job_response.json()["state"] in ["FULFILLED"]:
                logging.info("Job queued successfully.")
                break

        data_url = f"{job_url}/results"
        cursor = ""

        while True:

            # Request
            request_url = f"{data_url}?cursor={cursor}" if cursor else data_url
            response = requests.get(request_url, headers=header)

            # Data parser
            if endpoint == "user_detail_job":
                self.parse_user_data(response.json())
            elif endpoint == "conversation_detail_job":
                self.parse_conversation_data(response.json())

            # Pagination
            if "cursor" in response.json():
                cursor = response.json()["cursor"]
            else:
                break


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
    except Exception as exc:
        logging.exception(exc)
        exit(1)
